package jsonschema

import (
	"fmt"
)

/****************************
 * Exported structs/methods *
 ****************************/

// Fake an enum
var DiffTypes = &diffTypes{
	TypeChanged:      1,
	PropertyRequired: 2,
	PropertyOptional: 3,
	PropertyAdded:    4,
	PropertyRemoved:  5,
}

var DiffDescriptions = [...]string{
	"No Change",
	"Type changed",
	"Property required",
	"Property optional",
	"Property added",
	"Property removed",
}

type Diff struct {
	// Type of diff, defined by constants above
	Type int `json:"type,omitempty"`

	// Human-readable description of the diff
	Description string `json:"description,omitempty"`

	// JSON path to the property (not including property)
	Path string `json:"path,omitempty"`

	// Type of property for both a and b
	TypeA string `json:"typeA,omitempty"`
	TypeB string `json:"typeB,omitempty"`

	// Version change caused by diff according to the ruleset
	Version string `json:"version,omitempty"`
}

// Diff 2 json schemas
func DiffSchemas(aSchema, bSchema map[string]interface{}) ([]Diff, error) {
	return diffSchemas(aSchema, bSchema, "root", "root")
}

/****************************
 * Internal structs/methods *
 ****************************/

type diffTypes struct {
	TypeChanged      int
	PropertyRequired int
	PropertyOptional int
	PropertyAdded    int
	PropertyRemoved  int
}

// Get json type for unmarshaled json
// According to docs: https://godoc.org/encoding/json#Unmarshal
func getType(v interface{}) (string, error) {
	switch v.(type) {
	case map[string]interface{}:
		return "object", nil
	case []interface{}:
		return "array", nil
	case string:
		return "string", nil
	case float64:
		return "number", nil
	case bool:
		return "boolean", nil
	}
	return "", fmt.Errorf("Type %T not supported!", v)
}

// Check if a slice contains an element
func sliceContains(s []interface{}, e interface{}) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// Get the min of 2 integers
func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Recursive function to diff json schemas, returning an array of the diffs between the schemas.
// This function supports adding and removing object properties and array item types, changing
// types, and required fields.
func diffSchemas(
	a map[string]interface{},
	b map[string]interface{},
	// The current json property
	property string,
	// The current json path
	jsonPath string,
) ([]Diff, error) {

	// The final output will be an array of diffs between the schemas
	diffs := []Diff{}

	// Get the json schema "type" of each schema object
	aType := a["type"].(string)
	bType := b["type"].(string)

	// If the types are different, create a diff a return as there's no need to try diffing any
	// further
	if aType != bType {
		diff := Diff{
			Type:        DiffTypes.TypeChanged,
			Description: DiffDescriptions[DiffTypes.TypeChanged],
			Path:        jsonPath,
			TypeA:       aType,
			TypeB:       bType,
		}
		diffs = append(diffs, diff)

		return diffs, nil
	}

	// By this point, the types are the same, so it's safe to switch on just the aType
	switch aType {

	// For objects, check for required/optional properties and recurse for each property
	case "object":
		aProps := a["properties"].(map[string]interface{})
		bProps := b["properties"].(map[string]interface{})

		// Get required vars which may or may not exist
		aReq, aOk := a["required"].([]interface{})
		bReq, bOk := b["required"].([]interface{})

		// Keep arrays of any new required or optional properties
		newReq := []interface{}{}
		newOpt := []interface{}{}

		// If a has required properties and b does not, it's likely that properties have been made
		// optional
		if aOk && !bOk {
			// Anything that was required in a is now optional
			newOpt = append(newOpt, aReq...)

			// If b has required properties and a does not, it's likely that properties have been made
			// required
		} else if !aOk && bOk {
			// Add the required properties from b
			newReq = append(newReq, bReq...)

			// If both a and b have required properties, compare the properties to see if any have been
			// added or removed
		} else if aOk && bOk {

			// Any properties exist in a but not in b, they are optional
			for _, p := range aReq {
				if !sliceContains(bReq, p) {
					newOpt = append(newOpt, p)
				}
			}

			// Likewise, if any properties exist in b but not in a, they are required
			for _, p := range bReq {
				if !sliceContains(aReq, p) {
					newReq = append(newReq, p)
				}
			}
		}

		// Create a diff for each required property
		for _, p := range newReq {
			// Don't add the diff if the property was removed
			if _, ok := bProps[p.(string)]; ok {
				diff := Diff{
					Type:        DiffTypes.PropertyRequired,
					Description: DiffDescriptions[DiffTypes.PropertyRequired],
					Path:        fmt.Sprintf("%s.%s", jsonPath, p.(string)),
				}
				diffs = append(diffs, diff)
			}
		}

		// Create a diff for each optional property
		for _, p := range newOpt {
			// Don't add the diff if the property was removed
			if _, ok := bProps[p.(string)]; ok {
				diff := Diff{
					Type:        DiffTypes.PropertyOptional,
					Description: DiffDescriptions[DiffTypes.PropertyOptional],
					Path:        fmt.Sprintf("%s.%s", jsonPath, p.(string)),
				}
				diffs = append(diffs, diff)
			}
		}

		// Iterate the properties of a to check for removed properties and recurse for any properties
		// that exist in both a and b
		for k, v := range aProps {

			// Create a diff for each removed property
			if bv, ok := bProps[k]; !ok {
				diff := Diff{
					Type:        DiffTypes.PropertyRemoved,
					Description: DiffDescriptions[DiffTypes.PropertyRemoved],
					Path:        fmt.Sprintf("%s.%s", jsonPath, k),
					TypeA:       aProps[k].(map[string]interface{})["type"].(string),
				}
				diffs = append(diffs, diff)

				// Otherwise, the properties exist in both a and b. Each property is itself a json schema and
				// must be recursed.
			} else {
				kDiffs, err := diffSchemas(
					v.(map[string]interface{}),
					bv.(map[string]interface{}),
					k,
					fmt.Sprintf("%s.%s", jsonPath, k),
				)
				if err != nil {
					return nil, err
				}

				diffs = append(diffs, kDiffs...)
			}
		}

		// Iterate the properties of b to check for any added properties. Any new properties that are
		// required will be caught by the required property check above.
		for k, _ := range bProps {

			// Create a diff for each added property
			if _, ok := aProps[k]; !ok {
				diff := Diff{
					Type:        DiffTypes.PropertyAdded,
					Description: DiffDescriptions[DiffTypes.PropertyAdded],
					Path:        fmt.Sprintf("%s.%s", jsonPath, k),
					TypeB:       bProps[k].(map[string]interface{})["type"].(string),
				}
				diffs = append(diffs, diff)
			}
		}

	// JSON schema arrays support a few different validation methods, but the most important are
	// arrays where all items match a single schema, and tuples where the array is a fixed size and
	// each ordered element has it's own schema. The former is represented by an items object and the
	// latter an items array of objects.
	case "array":

		// Get the JSON type of the array "items"
		aItemsType, err := getType(a["items"])
		if err != nil {
			return nil, err
		}
		bItemsType, err := getType(b["items"])
		if err != nil {
			return nil, err
		}

		// If a and b have the same items type, recursively compare the nested schemas
		if aItemsType == bItemsType {

			// Items can either be an array (tuple) or object
			switch aItemsType {

			case "array":

				// Type assert the items as an array
				aItems := a["items"].([]interface{})
				bItems := b["items"].([]interface{})

				aLen := len(aItems)
				bLen := len(bItems)
				minLen := min(aLen, bLen)

				// Diff the items objects where both a and b have a value
				for i := 0; i < minLen; i++ {
					itemDiffs, _ := diffSchemas(
						aItems[i].(map[string]interface{}),
						bItems[i].(map[string]interface{}),
						string(i),
						fmt.Sprintf("%s[%d]", jsonPath, i),
					)

					diffs = append(diffs, itemDiffs...)
				}

				// If a has more items than b, fields have been removed
				if aLen > bLen {
					aSlice := aItems[minLen:aLen]
					for i, v := range aSlice {
						diff := Diff{
							Type:        DiffTypes.PropertyRemoved,
							Description: DiffDescriptions[DiffTypes.PropertyRemoved],
							Path:        fmt.Sprintf("%s[%d]", jsonPath, minLen+i),
							TypeA:       v.(map[string]interface{})["type"].(string),
						}
						diffs = append(diffs, diff)
					}
				}

				// If a has less items than b, fields have been added
				if aLen < bLen {
					bSlice := bItems[minLen:bLen]
					for i, v := range bSlice {
						diff := Diff{
							Type:        DiffTypes.PropertyAdded,
							Description: DiffDescriptions[DiffTypes.PropertyAdded],
							Path:        fmt.Sprintf("%s[%d]", jsonPath, minLen+i),
							TypeB:       v.(map[string]interface{})["type"].(string),
						}
						diffs = append(diffs, diff)
					}
				}

			case "object":

				// Type assert items as objects
				aItems := a["items"].(map[string]interface{})
				bItems := b["items"].(map[string]interface{})

				// Recursively diff the items
				itemsDiffs, err := diffSchemas(aItems, bItems, property, jsonPath)
				if err != nil {
					return nil, err
				}

				diffs = append(diffs, itemsDiffs...)

			// Error on any edge case not supported yet
			default:
				return nil, fmt.Errorf("Array item type %s not supported!", aItemsType)
			}

			// If a and b do not have the same type of array items, it's a type change
		} else {
			diff := Diff{
				Type:        DiffTypes.TypeChanged,
				Description: DiffDescriptions[DiffTypes.TypeChanged],
				Path:        fmt.Sprintf("%s.%s", jsonPath, "items"),
				TypeA:       aItemsType,
				TypeB:       bItemsType,
			}
			diffs = append(diffs, diff)
		}
	}

	return diffs, nil
}
