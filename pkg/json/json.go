package json

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// Loads and unmarshals a json file using generic types
func Load(path string) (interface{}, error) {
	// File exists?
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, err
	}

	// Read file
	jsonStr, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	// Parse json
	var jsonObj interface{}
	err = json.Unmarshal(jsonStr, &jsonObj)
	if err != nil {
		return nil, err
	}

	return jsonObj, nil
}
