package semver

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/love.wearensr/auto-semver/pkg/jsonschema"
	"gopkg.in/yaml.v2"
)

type Semver struct {
	ChangeSet []jsonschema.Diff `json:"changeset,omitempty"`
	Version   string            `json:"version,omitempty"`
}

type version struct {
	Major int
	Minor int
	Patch int
}

var Version = &version{
	Major: 0,
	Minor: 1,
	Patch: 2,
}

type Config struct {
	PropertyAdded    string `yaml:"propertyAdded"`
	PropertyRemoved  string `yaml:"propertyRemoved"`
	PropertyRequired string `yaml:"propertyRequired"`
	PropertyOptional string `yaml:"propertyOptional"`
	TypeChanged      string `yaml:"typeChanged"`
}

func getVersion(curVer int, ver string) int {
	var verNum int
	switch ver {
	case "major":
		verNum = Version.Major
	case "minor":
		verNum = Version.Minor
	case "patch":
		verNum = Version.Patch
	}

	if verNum < curVer {
		return verNum
	}
	return curVer
}

func GetVersion(diffs []jsonschema.Diff, curVerStr string, rulsetPath string) (string, error) {
	configData, err := ioutil.ReadFile(rulsetPath)
	if err != nil {
		return "", err
	}

	config := Config{}
	err = yaml.Unmarshal([]byte(configData), &config)
	if err != nil {
		return "", err
	}

	curVer := Version.Patch
	for i, diff := range diffs {
		switch diff.Type {
		case jsonschema.DiffTypes.TypeChanged:
			diffs[i].Version = config.TypeChanged
			curVer = getVersion(curVer, config.TypeChanged)
		case jsonschema.DiffTypes.PropertyRequired:
			diffs[i].Version = config.PropertyRequired
			curVer = getVersion(curVer, config.PropertyRequired)
		case jsonschema.DiffTypes.PropertyOptional:
			diffs[i].Version = config.PropertyOptional
			curVer = getVersion(curVer, config.PropertyOptional)
		case jsonschema.DiffTypes.PropertyAdded:
			diffs[i].Version = config.PropertyAdded
			curVer = getVersion(curVer, config.PropertyAdded)
		case jsonschema.DiffTypes.PropertyRemoved:
			diffs[i].Version = config.PropertyRemoved
			curVer = getVersion(curVer, config.PropertyRemoved)
		}
	}

	var verStr string
	if curVerStr != "" {
		verArr := strings.Split(curVerStr, ".")
		majorVer, err := strconv.Atoi(verArr[0])
		if err != nil {
			return "", err
		}
		minorVer, err := strconv.Atoi(verArr[1])
		if err != nil {
			return "", err
		}
		patchVer, err := strconv.Atoi(verArr[2])
		if err != nil {
			return "", err
		}

		switch curVer {
		case Version.Major:
			verStr = fmt.Sprintf("%d.%d.%d", majorVer+1, 0, 0)
		case Version.Minor:
			verStr = fmt.Sprintf("%d.%d.%d", majorVer, minorVer+1, 0)
		case Version.Patch:
			verStr = fmt.Sprintf("%d.%d.%d", majorVer, minorVer, patchVer+1)
		}
	} else {
		switch curVer {
		case Version.Major:
			verStr = "major"
		case Version.Minor:
			verStr = "minor"
		case Version.Patch:
			verStr = "patch"
		}
	}

	output := Semver{
		ChangeSet: diffs,
		Version:   verStr,
	}

	jsonStr, err := json.Marshal(output)
	if err != nil {
		return "", err
	}

	return string(jsonStr), nil
}
