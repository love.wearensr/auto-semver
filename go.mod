module gitlab.com/love.wearensr/auto-semver

go 1.12

require (
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/cucumber/godog v0.10.0
	github.com/sergi/go-diff v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
