auto-semver
===========

Automatically semantically version all the things

## Overview

[Semantic versioning](https://semver.org) allows safe releases of software where the impact of the
release is known by observing the next semantic version. The changes are classified as either
`major`, `minor`, or `patch`. `auto-semver` enables completely automated semantic versioning of
software, eliminating the element of human error by detecting the type of changes automatically.

Shared software is often consumed through an interface which is essentially a contract with the
consumer. A contract may be easily represented as a JSON schema, and JSON schemas can be diffed to
determine the type of changes that have occurred in the contract, thereby detecting the impact of
the change to consumers. This change impact is represented as the next semantic version.

Some example contracts include:

- API contracts for microservices (requests/responses)
- Public interfaces for software libraries (function signatures, data structures, etc)
- CLI options and output structures for binaries
- Terraform module variables and outputs

Any software that can be represented as a JSON schema can ba automatically semantically versioned!

## Usage

```sh
auto-semver \
  -current-schema <current json schema> \
  -new-schema <new json schema> \
  -ruleset <ruleset yaml file> \
  -current-version <current semantic version>
```
