Feature: Automatic semantic versioning

  Scenario: Add a property to an object
    Given I "add a property to an object"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Remove a property from an object
    Given I "remove a property from an object"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Change the type of an object property
    Given I "change the type of an object property"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Make an object property required
    Given I "make an object property required"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Make an object property optional
    Given I "make an object property optional"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Add a required property to an object
    Given I "add a required property to an object"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Remove a required property from an object
    Given I "remove a required property from an object"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Change an array items type
    Given I "change an array items type"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Add an item to a tuple
    Given I "add an item to a tuple"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Remove an item from a tuple
    Given I "remove an item from a tuple"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Change an item in a tuple
    Given I "change an item in a tuple"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Make multiple changes
    Given I "make multiple changes"
    When I automatically semantically version
    Then the results should be correct

  Scenario: Major version bump
    Given I "make a major change" with version "1.2.3"
    When I automatically semantically version
    Then I expect the version to be "2.0.0"

  Scenario: Minor version bump
    Given I "make a minor change" with version "1.2.3"
    When I automatically semantically version
    Then I expect the version to be "1.3.0"

  Scenario: Patch version bump
    Given I "make a patch change" with version "1.2.3"
    When I automatically semantically version
    Then I expect the version to be "1.2.4"


