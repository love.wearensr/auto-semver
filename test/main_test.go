package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"regexp"
	"sort"
	"strings"

	"github.com/cucumber/godog"
	"github.com/andreyvit/diff"
)

type Change struct {
	Type        int    `json:"type,omitempty"`
	Description string `json:"description,omitempty"`
	Path        string `json:"path,omitempty"`
	Version     string `json:"version,omitempty"`
	TypeA       string `json:"typeA,omitempty"`
	TypeB       string `json:"typeB,omitempty"`
}

type Results struct {
	Changeset []Change `json:"changeset,omitempty"`
	Version   string   `json:"version,omitempty"`
}

// Sort interface for a changeset
type ByChange []Change

func (a ByChange) Len() int      { return len(a) }
func (a ByChange) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (changes ByChange) Less(i, j int) bool {
	a := changes[i]
	b := changes[j]
	aStr := fmt.Sprintf("%d%s%s%s%s", a.Type, a.Path, a.Version, a.TypeA, a.TypeB)
	bStr := fmt.Sprintf("%d%s%s%s%s", b.Type, b.Path, b.Version, b.TypeA, b.TypeB)
	return aStr < bStr
}

var testPath string
var receivedStr []byte
var curVer string

func i(arg1 string) error {
	slugRegex, err := regexp.Compile("[^0-9a-z_]")
	if err != nil {
		return err
	}
	testDir := slugRegex.ReplaceAllString(strings.ToLower(arg1), "_")
	testPath = path.Join("fixtures", testDir)
	if _, err := os.Stat(testPath); os.IsNotExist(err) {
		return fmt.Errorf("Test fixture not found: %s", testPath)
	}
	return nil
}

func iAutomaticallySemanticallyVersion() error {
	aJsonPath := path.Join(testPath, "a.json")
	bJsonPath := path.Join(testPath, "b.json")
	rulesetPath := path.Join(testPath, "ruleset.yaml")

	var cmd *exec.Cmd
	if len(curVer) > 0 {
		cmd = exec.Command("auto-semver", "-current-schema", aJsonPath, "-new-schema", bJsonPath, "-ruleset", rulesetPath, "-current-version", curVer)
	} else {
		cmd = exec.Command("auto-semver", "-current-schema", aJsonPath, "-new-schema", bJsonPath, "-ruleset", rulesetPath)
	}
	results, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("%s", results)
	}

	receivedStr = results
	return nil
}

func theResultsShouldBeCorrect() error {
	expectedPath := path.Join(testPath, "expected.json")
	expectedStr, err := ioutil.ReadFile(expectedPath)
	if err != nil {
		return err
	}

	var expected Results
	var received Results
	err = json.Unmarshal(expectedStr, &expected)
	if err != nil {
		return err
	}
	err = json.Unmarshal(receivedStr, &received)
	if err != nil {
		return err
	}

	if received.Version != expected.Version {
		return fmt.Errorf("Expected version \"%s\" to be \"%s\"", received.Version, expected.Version)
	}

	sort.Sort(ByChange(received.Changeset))
	sort.Sort(ByChange(expected.Changeset))

	receivedStr, err = json.MarshalIndent(received, "", "  ")
	if err != nil {
		return err
	}

	expectedStr, err = json.MarshalIndent(expected, "", "  ")
	if err != nil {
		return err
	}

	if string(receivedStr) != string(expectedStr) {
		fmt.Printf("%v", diff.LineDiff(string(receivedStr), string(expectedStr)))
		return fmt.Errorf("Expected the results to match")
	}

	return nil
}

func iWithVersion(arg1, arg2 string) error {
	i(arg1)
	curVer = arg2
	return nil
}

func iExpectTheVersionToBe(arg1 string) error {
	var received Results
	err := json.Unmarshal(receivedStr, &received)
	if err != nil {
		return err
	}

	if received.Version != arg1 {
		return fmt.Errorf("Expected version \"%s\" to be \"%s\"", received.Version, arg1)
	}

	return nil
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^I "([^"]*)"$`, i)
	s.Step(`^I automatically semantically version$`, iAutomaticallySemanticallyVersion)
	s.Step(`^the results should be correct$`, theResultsShouldBeCorrect)
	s.Step(`^I "([^"]*)" with version "([^"]*)"$`, iWithVersion)
	s.Step(`^I expect the version to be "([^"]*)"$`, iExpectTheVersionToBe)
}
