# Multi-stage build, with `alpine` responsible for grabbing and verifying `jq` binary
FROM alpine as loader

RUN apk add \
  gnupg \
  wget

# Install jq
ENV JQ_VERSION='1.6'

RUN wget --no-check-certificate https://raw.githubusercontent.com/stedolan/jq/master/sig/jq-release.key -O /tmp/jq-release.key && \
    wget --no-check-certificate https://raw.githubusercontent.com/stedolan/jq/master/sig/v${JQ_VERSION}/jq-linux64.asc -O /tmp/jq-linux64.asc && \
    wget --no-check-certificate https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -O /tmp/jq && \
    gpg --import /tmp/jq-release.key && \
    gpg --verify /tmp/jq-linux64.asc /tmp/jq && \
    chmod +x /tmp/jq

# Reset to end up with a container that only runs `auto-semver` and `jq`
FROM scratch

# Copy files
COPY --from=loader tmp/jq /bin/jq
COPY ./build/auto-semver /bin/auto-semver

# Set default comparison entrypoint
CMD [ "/bin/auto-semver" ]