package main

import (
	"flag"
	"fmt"

	"gitlab.com/love.wearensr/auto-semver/pkg/json"
	"gitlab.com/love.wearensr/auto-semver/pkg/jsonschema"
	"gitlab.com/love.wearensr/auto-semver/pkg/semver"
)

type args struct {
	CurrentSchema  string
	NewSchema      string
	CurrentVersion string
	Ruleset        string
}

func parseArgs() args {
	curSchemaPtr := flag.String("current-schema", "", "Current JSON schema")
	newSchemaPtr := flag.String("new-schema", "", "New JSON schema")
	curVerPtr := flag.String("current-version", "", "Current semantic version")
	rulesetPtr := flag.String("ruleset", "", "Ruleset YAML")

	flag.Parse()

	return args{
		CurrentSchema:  *curSchemaPtr,
		NewSchema:      *newSchemaPtr,
		CurrentVersion: *curVerPtr,
		Ruleset:        *rulesetPtr,
	}
}

func main() {
	args := parseArgs()

	aSchema, err := json.Load(args.CurrentSchema)
	if err != nil {
		panic(err)
	}
	bSchema, err := json.Load(args.NewSchema)
	if err != nil {
		panic(err)
	}

	diffs, err := jsonschema.DiffSchemas(
		aSchema.(map[string]interface{}),
		bSchema.(map[string]interface{}),
	)
	if err != nil {
		panic(err)
	}

	jsonStr, err := semver.GetVersion(diffs, args.CurrentVersion, args.Ruleset)
	if err != nil {
		panic(err)
	}

	fmt.Println(jsonStr)
}
