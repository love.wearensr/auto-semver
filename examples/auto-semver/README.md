auto-semver
===========

This example can be used to automatically semantically version the `auto-semver` project by
converting the CLI inputs and the output structure into JSON schemas and using `auto-semver` to diff
the changes.

## Prerequisites

- auto-semver
- [jq](https://stedolan.github.io/jq/)

## Usage

```sh
auto-semver -current-schema a.json -new-schema b.json -ruleset ruleset.yaml | jq
```

Output:

```json
{
  "changeset": [
    {
      "type": 2,
      "description": "Property required",
      "path": "root.inputs.ruleset",
      "version": "major"
    },
    {
      "type": 4,
      "description": "Property added",
      "path": "root.inputs.ruleset",
      "typeB": "string",
      "version": "minor"
    },
    {
      "type": 5,
      "description": "Property removed",
      "path": "root.outputs.changeset.description",
      "typeA": "string",
      "version": "major"
    }
  ],
  "version": "major"
}
```
